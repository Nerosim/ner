require "string"
require "apache2"

local sqlite3 = require("lsqlite3")
local myDB = sqlite3.open("mydb.db")

function sqlarg(arg, r)
    sql = " '" .. r:escape_logitem(arg) .. "' "
    return sql
end

function handle(r)
    r.content_type = "text/html; charset=UTF-8"

    local User
    if r:getcookie("User") == nil then
        User = "_"
    else
        User = r:getcookie("User")
    end

    local UserL
    if r:getcookie("UserL") == nil then
        UserL = "-"
    else
        UserL = r:getcookie("UserL")
    end

    r:sendfile("C:/Apache24/htdocs/Nero_interface.html")

    local sql_user = "(SELECT Login FROM 'User' WHERE Login = '" .. UserL .. "')"
    local sql_user_id = "(SELECT Id_User FROM 'User' WHERE Login = '" .. UserL .. "')"
    local sql = "SELECT number_of_layers FROM Network_design_T WHERE name_t='layer-start' AND Id_User ="
    r:write("<script>\n")

    local R, Res, sum_leyer, cout_data, cout_leyer, cout_data_Forecast, sum_input_data

    for R in myDB:urows(sql .. sql_user) do
        res = R
    end

    r:write("let data_count_1 = " .. res .. ";\n")

    sql = "SELECT number_of_layers FROM Network_design_T WHERE name_t='layer-end' AND Id_User ="

    for R in myDB:urows(sql .. sql_user) do
        res = R
    end

    r:write("let data_count_2 = " .. res .. ";\n")

    r:write("</script>\n")

    sql =
        "SELECT sum( number_of_layers) FROM Network_design_T WHERE  (name_t='layer-start' OR name_t='layer-end') AND Id_User = "

    for R in myDB:urows(sql .. sql_user) do
        sum_leyer = R
    end

    sql = "SELECT COUNT(name_t) FROM Network_Training_T WHERE Id_User = "

    for R in myDB:urows(sql .. sql_user) do
        cout_data = R
    end

    sql = "SELECT COUNT(name_t) FROM Network_Forecast_data_T WHERE name_t LIKE 'R_Data_g%' AND Id_User = "

    for R in myDB:urows(sql .. sql_user) do
        cout_data_Forecast = R
    end

    sql = "SELECT COUNT(name_t) FROM Network_design_T WHERE name_t LIKE 'Hidden_layer_%' AND Id_User = "

    for R in myDB:urows(sql .. sql_user) do
        cout_leyer = R
    end

    sql = "SELECT number_of_layers FROM Network_design_T WHERE name_t='layer-start' AND Id_User= "

    for R in myDB:urows(sql .. sql_user) do
        sum_input_data = R
    end

    local N, V

    r:write("<script>\n")
    r:write("load_data_leyer(" .. cout_leyer .. ");\n")
    r:write("load_data_Forecast(" .. "(" .. cout_data_Forecast .. " / " .. sum_input_data .. ")" .. ");\n")
    r:write("</script>\n")

    r:write("<script>\n")
    r:write("load_data_1(" .. (cout_data / sum_leyer) .. ");\n")
    r:write("</script>\n")

    sql = "SELECT name_t, number_of_layers FROM Network_design_T WHERE Id_User = "

    r:write("<script>\n")

    for N, V in myDB:urows(sql .. sql_user) do
        r:write("document.getElementsByName('" .. N .. "')[0].value = '" .. V .. "';\n")
    end
    r:write("</script>\n")

    sql = "SELECT name_t, DATA FROM Network_Training_T WHERE Id_User = "

    for N, V in myDB:urows(sql .. sql_user) do
        r:write("<script>\n")
        r:write("document.getElementsByName('" .. N .. "')[0].value = '" .. V .. "';\n")
        r:write("</script>\n")
    end

    sql = "SELECT name_t, DATA FROM Network_Forecast_data_T WHERE Id_User = "

    for N, V in myDB:urows(sql .. sql_user) do
        r:write("<script>\n")
        r:write("document.getElementsByName('" .. N .. "')[0].value = '" .. V .. "';\n")
        r:write("</script>\n")
    end

    sql = "SELECT id_graph, data FROM Network_graph_T WHERE Id_User= "

    r:write("<script>\n")
    local xValues, yValues

    xValues = "const xValues = ["
    yValues = "const yValues = ["

    for N, V in myDB:urows(sql .. sql_user_id) do
        xValues = xValues .. N .. ", "
        yValues = yValues .. math.abs(V * 100) .. ", "
    end

    r:write(xValues:sub(1, -3) .. "];" .. "\n")
    r:write(yValues:sub(1, -3) .. "];" .. "\n")

    r:sendfile("C:/Apache24/cgi-bin/Graph.js")

    r:write("</script>\n")

    myDB:close()
    return apache2.OK
end

