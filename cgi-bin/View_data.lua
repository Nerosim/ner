function handle(r)
    r.content_type = "text/html; charset=UTF-8"
    local T_arg_request = {}
    local Request_file
    local Request_str
    local Start_file
    local End_file
    local array

    if r.method == "POST" then
        Request_str = r:requestbody()

        local Request_arg = string.match(Request_str, "[^\n]+\n[^\n]+\n[^\n]+\n.\n.")

        Start_file = string.len(Request_arg)
        End_file = #Request_str - #string.match(string.reverse(Request_str), "..%-%-%d+%-+..")

        local s = Request_arg
        for k, v in string.gmatch(s, '(%w+)="([^"]+)"') do
            T_arg_request[k] = v
        end

        Request_file = string.sub(Request_str, Start_file, End_file)
    end

    r:puts("<!DOCTYPE html>")
    r:puts("<html>")
    r:puts("<head>")

    r:sendfile("C:/Apache24/cgi-bin/table.css")

    r:puts("</head>")
    r:puts("<body>")
    r:puts("<div>" .. T_arg_request["filename"] .. ":</div>\n")

    local i = 1
    local T_numberi = {}
    local T_numbero = {}
    local No = 1

    for data in string.gmatch(Request_file, "data:%[[^%]]-%]") do
        r:puts('<table class="btable" id="Numt' .. No .. '">' .. "\n")

        r:puts("<tr>" .. "\n")
        r:puts("<th> " .. No .. " </th>" .. "\n")
        r:puts('<th onclick="sortTable(1,\'' .. "Numt" .. No .. '\')"> Input </th>' .. "\n")
        r:puts('<th onclick="sortTable(2,\'' .. "Numt" .. No .. '\')">  Output </th>' .. "\n")
        r:puts("</tr>" .. "\n")
        T_numberi = {}
        i = 1
        array = string.match(data, "input:{([^}]-)}")
        for number in string.gmatch(array, "[+-]?%d*%.?%d+") do
            T_numberi[i] = number
            i = i + 1
        end
        T_numbero = {}
        i = 1
        array = string.match(data, "output:{([^}]-)}")
        for number in string.gmatch(array, "[+-]?%d*%.?%d+") do
            T_numbero[i] = number
            i = i + 1
        end

        for i = 1, #T_numbero < #T_numberi and #T_numberi or #T_numbero do
            r:puts("<tr>" .. "\n")
            r:puts("<td></td>" .. "\n")
            r:puts("<td>" .. (T_numberi[i] ~= nil and T_numberi[i] or "_") .. "</td>" .. "\n")
            r:puts("<td>" .. (T_numbero[i] ~= nil and T_numbero[i] or "_") .. "</td>" .. "\n")
            r:puts("</tr>" .. "\n")
        end
        No = No + 1
        r:puts("</table>" .. "\n")
    end
    r:sendfile("C:/Apache24/cgi-bin/sort_table.js")
    r:puts("</body>")
    r:puts("</html>")

    return apache2.OK
end

