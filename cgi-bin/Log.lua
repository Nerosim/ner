
require "string"
require "apache2"

local sqlite3 = require("lsqlite3")
local myDB = sqlite3.open("mydb.db")

function sqlarg(arg, r)
    sql = " '" .. r:escape_logitem(arg) .. "' "
    return sql
end

function handle(r)
    r.content_type = "text/html; charset=UTF-8"

    r:setcookie {
        key = "User",
        value = "",
        expires = 1,
        secure = true
    }

    r:setcookie {
        key = "UserL",
        value = "",
        expires = 1,
        secure = true
    }

    local password_field = "password-field"
    local Login = "Login"
    local g_recaptcha = "g-recaptcha-response"

    local IP = r.useragent_ip

    if r.method == "POST" then
        for k, v in pairs(r:parsebody()) do
            if k == password_field then
                password_field = v
            end
            if k == Login then
                Login = v
            end
            if k == g_recaptcha then
                g_recaptcha = v
            end
        end
    end

    local sql =
        'SELECT EXISTS(SELECT * FROM "User" WHERE Login = ' ..
        "'" .. Login .. "'" .. " and " .. "password_field = " .. "'" .. r:md5(password_field) .. "'" .. ");"
    local output

    for R in myDB:urows(sql) do
        output = R
    end

    if output == 1 and g_recaptcha ~= "" then
        r:setcookie {
            key = "User",
            value = r:md5(password_field),
            expires = 0,
            secure = true
        }
    end

    if output == 1 and g_recaptcha ~= "" then
        r:setcookie {
            key = "UserL",
            value = Login,
            expires = 0,
            secure = true
        }
    end

    myDB:close()
    r:write("<script> location.href = 'http://localhost/'; </script>")

    return apache2.OK
end

