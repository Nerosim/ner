require "apache2"

local sqlite3 = require("lsqlite3")
local myDB = sqlite3.open("mydb.db")

function handle(r)
    r.content_type = "text/html; charset=UTF-8"

    local User
    if r:getcookie("User") == nil then
        User = "_"
    else
        User = r:getcookie("User")
    end

    local UserL
    if r:getcookie("UserL") == nil then
        UserL = "-"
    else
        UserL = r:getcookie("UserL")
    end

    local sql_user = "(SELECT Login FROM 'User' WHERE Login = '" .. UserL .. "')"
    local sql_user_id = "(SELECT Id_User FROM 'User' WHERE Login = '" .. UserL .. "')"
    local sql_del = "DELETE FROM Network_Training_T WHERE Id_User=" .. sql_user

    myDB:exec(sql_del)

    local sql = "INSERT INTO Network_Training_T (name_t, data, Id_User) VALUES('"

    local i = 1

    if r.method == "POST" then
        local POST, POSTMULTI = r:parsebody()

        while (POST["T_Data_g" .. i] ~= nil) do
            myDB:exec(
                sql ..
                    "T_Data_g" ..
                        i ..
                            "', " ..
                                (POST["T_Data_g" .. i] == "" and "0" or POST["T_Data_g" .. i]) ..
                                    ", " .. sql_user .. ")"
            )
            i = i + 1
        end

        i = 1

        while (POST["T_Data_r" .. i] ~= nil) do
            myDB:exec(
                sql ..
                    "T_Data_r" ..
                        i ..
                            "', " ..
                                (POST["T_Data_r" .. i] == "" and "0" or POST["T_Data_r" .. i]) ..
                                    ", " .. sql_user .. ")"
            )
            i = i + 1
        end

        local R, Res

        for R in myDB:urows("select" .. sql_user_id) do
            res = R
        end

        os.execute("Nero\\nero_K.exe -u " .. res .. " -s " .. POST["Speed"] .. " -e " .. POST["Steps"])
    end

    myDB:close()
    r:write("<script> location.href = 'http://localhost/cgi-bin/Nero_interface_script_bd.lua'; </script>")
    return apache2.OK
end

