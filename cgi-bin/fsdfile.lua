local sqlite3 = require("lsqlite3")
local myDB=sqlite3.open('mydb.db')

function sqlarg(arg,r)
sql=" '"..r:escape_logitem(arg).."' "
return sql
end

function tohex(str)
    return (str:gsub('.',function(c)return string.format('%02X',string.byte(c))end))
end

function handle(r)
r.content_type = "text/html; charset=UTF-8"
local T_arg_request = {}
local Request_file
local Request_str
local Start_file
local End_file

if r.method == 'POST' then

     Request_str = r:requestbody()

     local Request_arg = string.match(Request_str, "[^\n]+\n[^\n]+\n[^\n]+\n.\n.")

     Start_file = string.len(Request_arg)
     End_file = #Request_str-#string.match(string.reverse(Request_str), "..%-%-%d+%-+..")

     local s = Request_arg
     for k, v in string.gmatch(s, "(%w+)=\"([^\"]+)\"") do
       T_arg_request[k] = v
     end

     Request_file = string.sub(Request_str,Start_file,End_file)
end

local User
if r:getcookie("User")==nil then
User = "_"
else
User = r:getcookie("User") end

local UserL
if r:getcookie("UserL")==nil then
UserL = "-"
else
UserL = r:getcookie("UserL") end

local sql

if T_arg_request["filename"]~=nil and End_file<=66000 then
   sql = "UPDATE 'User' SET ".."image".." = X'"..(tohex(Request_file)).."' WHERE password_field = "..sqlarg(User,r).." and Login = "..sqlarg(UserL,r)..";"
   myDB:exec(sql)
end

myDB:close()
r:write("<script> location.href = 'http://localhost/cgi-bin/Pre_fsd.lua'; </script>")
    return apache2.OK
end
