# Ner

## Name
Prim-Neural-Sim

## Description
Interface to a neural network and training with the ability to learn by introducing incoming and outgoing signals with displaying graphs of training effectiveness.

## Authors and acknowledgment
I express my gratitude to the following projects:
Lua,C++,javascript,HTML,LuaSQLite3,SQLite,Apache,Apache Module mod_lua,cURL,JQuery,Chart.js,STL,Armadillo 
Conrad Sanderson and Ryan Curtin.
Armadillo: a template-based C++ library for linear algebra.
Journal of Open Source Software, Vol. 1, No. 2, pp. 26, 2016.
Conrad Sanderson and Ryan Curtin.
Practical Sparse Matrices in C++ with Hybrid Storage and Template-Based Expression Optimization.
Mathematical and Computational Applications, Vol. 24, No. 3, 2019. morrisfranken /argparse,GrapesJS 

## Project status
Development has stopped completely