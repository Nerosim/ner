CREATE TABLE User (
	Id_User INTEGER PRIMARY KEY AUTOINCREMENT,
	Fname TEXT,
	Gender TEXT,
	Age INTEGER,
	phone TEXT,
	email TEXT,
	country TEXT,
	password_field TEXT,
	Login TEXT UNIQUE 
, image BLOB, Birthday TEXT);

CREATE TABLE Network_design_T (
	name_t TEXT,
	number_of_layers INTEGER,
	Id_User INTEGER,
	CONSTRAINT Network_design_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);

CREATE TABLE Network_Training_T (
	name_t TEXT,
	data REAL,
	Id_User INTEGER,
	CONSTRAINT Network_Training_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);

CREATE TABLE Network_Forecast_data_T (
	name_t TEXT,
	data REAL,
	Id_User INTEGER,
	CONSTRAINT Network_Training_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);

CREATE TABLE Network_Weights_M (
id_Matrix INTEGER,
Matrix_W BLOB,
	Id_User INTEGER,
	CONSTRAINT Network_Training_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);

CREATE TABLE Network_Bias_M (
id_Matrix INTEGER,
Matrix_B BLOB,
	Id_User INTEGER,
	CONSTRAINT Network_Training_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);

CREATE TABLE Network_graph_T (
	id_graph INTEGER,
	data REAL,
	Id_User INTEGER,
	CONSTRAINT Network_Training_T_User_FK FOREIGN KEY (Id_User) REFERENCES "User"(Id_User) ON DELETE SET DEFAULT ON UPDATE SET DEFAULT
);
