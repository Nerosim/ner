//============================================================================
/*

888b    888                                    888                         888                                   888                                       d8b                          .d888 d8b 888                   
8888b   888                                    888                         888                                   888                                       Y8P                         d88P"  Y8P 888                   
88888b  888                                    888                         888                                   888                                                                   888        888                   
888Y88b 888  .d88b.  888  888 888d888  8888b.  888       88888b.   .d88b.  888888 888  888  888  .d88b.  888d888 888  888       .d8888b   8888b.  888  888 888 88888b.   .d88b.        888888 888 888  .d88b.  .d8888b  
888 Y88b888 d8P  Y8b 888  888 888P"       "88b 888       888 "88b d8P  Y8b 888    888  888  888 d88""88b 888P"   888 .88P       88K          "88b 888  888 888 888 "88b d88P"88b       888    888 888 d8P  Y8b 88K      
888  Y88888 88888888 888  888 888     .d888888 888       888  888 88888888 888    888  888  888 888  888 888     888888K        "Y8888b. .d888888 Y88  88P 888 888  888 888  888       888    888 888 88888888 "Y8888b. 
888   Y8888 Y8b.     Y88b 888 888     888  888 888       888  888 Y8b.     Y88b.  Y88b 888 d88P Y88..88P 888     888 "88b            X88 888  888  Y8bd8P  888 888  888 Y88b 888       888    888 888 Y8b.          X88 
888    Y888  "Y8888   "Y88888 888     "Y888888 888       888  888  "Y8888   "Y888  "Y8888888P"   "Y88P"  888     888  888        88888P' "Y888888   Y88P   888 888  888  "Y88888       888    888 888  "Y8888   88888P' 
                                                                                                                                                                             888                                        
                                                                                                                                                                        Y8b d88P                                        
                                                                                                                                                                         "Y88P"                                         
 */
//============================================================================

#include "argparse/argparse.hpp"
#include <armadillo>
#include <fstream>
#include <iostream>
#include <math.h>
#include <random>
#include <sqlite3.h>
#include <vector>

using namespace std;
using namespace arma;

string F_Hex(char *F) {

  ifstream input(F, ios::binary);

  vector<unsigned char> buffer(istreambuf_iterator<char>(input), {});
  input.close();

  char *hexStr = new char[buffer.size() * 2];
  char bufferstr[2];

  int j = 0;
  size_t sbi = 0;

  for (auto &S : buffer) {
    sprintf(hexStr + j, "%02X", (int)S);
    j += 2;
    sbi++;
  }
  hexStr[j] = '\0';

  string Return(hexStr);

  delete[] hexStr;

  return Return;
}

void Hex_F(string hexStr, char *F) {
  string hs1, hs2;

  ofstream myfile(F, ios::binary);

  size_t Hlen = hexStr.size();

  for (size_t i = 0; i < Hlen; i += 2) {

    hs1 = hexStr[i];
    hs2 = hexStr[i + 1];

    myfile << (char)strtol((hs1 + hs2).c_str(), NULL, 16);
  }

  myfile.close();
}

mat Nor(mat X, double Min, double Max) {
  mat F = X;

  for (int i = 0; i < X.n_elem; i++) {
    F[i] = (X[i] - Min) / (Max - Min);
  }

  return F;
}

mat ffmat(mat X, double (*f)(double)) {
  mat F = X;

  for (int i = 0; i < X.n_elem; i++) {
    F[i] = f(X[i]);
  }

  return F;
}

double dxtanh(double x) { return 1 - (x) * (x); }

vector<mat> nerO(mat I, vector<mat> V, vector<mat> B, int size) {
  vector<mat> Ov;
  mat O = ffmat(I * V[0] + B[0], tanh);
  Ov.push_back(O);

  for (int i = 1; i < size; i++) {
    O = ffmat(O * V[i] + B[i], tanh);
    Ov.push_back(O);
  }
  return Ov;
}

string sql_get(sqlite3 *DB, string query) {
  sqlite3_stmt *stmt;
  if (sqlite3_prepare_v2(DB, query.c_str(), -1, &stmt, nullptr) != SQLITE_OK) {
    return "Error!";
  }

  string res;

  if (sqlite3_step(stmt) == SQLITE_ROW) {
    const char *C =
        reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    res = C;
  }

  sqlite3_finalize(stmt);
  return res;
}

struct MyArgs : public argparse::Args {

  int &id_user = kwarg("u,user", "Id user database").set_default(1);
  bool &verbose = flag("v,verbose", "A flag to toggle verbose");
};

void load_stuct(sqlite3 *DB, vector<int> *Ln, string sql_user) {
  string query1 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-start'"
                  "AND Id_User=";
  query1 += sql_user;

  string query2 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-end'"
                  "AND Id_User=";
  query2 += sql_user;

  Ln->push_back(stoi(sql_get(DB, query1.c_str())));

  for (int i = 1; i < 10; i++) {
    string queryn =
        "SELECT number_of_layers FROM Network_design_T WHERE name_t=";
    string res = sql_get(DB, queryn + "'Hidden_layer_" + to_string(i) +
                                 "' AND Id_User=" + sql_user);

    if (res.size() == 0) {
      break;
    }
    Ln->push_back(stoi(res));
  }

  Ln->push_back(stoi(sql_get(DB, query2.c_str())));
}

void load_I_data(sqlite3 *DB, vector<int> *Ln, string sql_user, vector<mat> *Vi,
                 int Min, int Max) {
  string res;
  string sql =
      "SELECT data FROM Network_Forecast_data_T WHERE name_t='R_Data_g";

  int Gn = 1;

  for (int i = 0;; i++) {

    mat I(1, (*Ln)[0], fill::ones);

    for (int j = 0; j < (*Ln)[0]; j++) {
      res = (sql + to_string(Gn) + "' and Id_User=" + sql_user);

      I[j] = atof(sql_get(DB, res).c_str());
      Gn++;
    }

    if (sql_get(DB, res).size() == 0) {
      break;
    }

    Vi->push_back(Nor(I, Min, Max));
  }
}

int main(int argc, char *argv[]) {

  auto args = argparse::parse<MyArgs>(argc, argv);

  if (args.verbose)
    args.print(); // prints all variables

  sqlite3 *DB;
  sqlite3_open("mydb.db", &DB);

  string sql_user = "(SELECT Login FROM 'User' WHERE Id_User = ";
  sql_user = sql_user + to_string(args.id_user) + ")";

  string sql_user_id = to_string(args.id_user);

  arma_rng::set_seed_random();

  vector<int> Ln;

  load_stuct(DB, &Ln, sql_user);

  int size = Ln.size();

  vector<mat> Vi;

  int Min = -1;
  int Max = 1;

  load_I_data(DB, &Ln, sql_user, &Vi, Min, Max);

  vector<mat> V;
  vector<mat> B;

  vector<mat> Ov;

  mat Oz;
  mat Vz;
  mat De;
  vector<mat> Nv;
  vector<mat> Nb;

  string sql = "SELECT hex(Matrix_W) FROM Network_Weights_M WHERE Id_User= " +
               sql_user_id;
  string res;
  mat buf;
  for (size_t i = 0; i < size - 1; i++) {
    res = sql_get(DB, sql + " and id_Matrix=" + to_string(i));

    Hex_F(res, "M.bin");
    buf.load("M.bin");
    V.push_back(buf);
  }

  sql =
      "SELECT hex(Matrix_B) FROM Network_Bias_M WHERE Id_User= " + sql_user_id;

  for (size_t i = 0; i < size - 1; i++) {
    res = sql_get(DB, sql + " and id_Matrix=" + to_string(i));

    Hex_F(res, "M.bin");
    buf.load("M.bin");
    B.push_back(buf);
  }

  sql = "INSERT INTO Network_Forecast_data_T (name_t, data, Id_User) VALUES('R_Data_r";
  int G = 1;

  for (size_t i = 0; i < Vi.size(); i++) {
    Ov = nerO(Vi[i], V, B, size - 1);
    buf = (Ov[size - 2] * (Max - Min) + Min);

    for (size_t j = 0; j < buf.n_elem; j++) {
      res = sql + to_string(G) + "', " + to_string(buf[j]) + ", "+sql_user+");";
      sqlite3_exec(DB, res.c_str(), NULL, NULL, NULL);

      G++;
    }
  }

  return 0;
}
