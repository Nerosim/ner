//============================================================================
/*

888b    888                                    888                         888                                   888                                               888    d8b                   
8888b   888                                    888                         888                                   888                                               888    Y8P                   
88888b  888                                    888                         888                                   888                                               888                          
888Y88b 888  .d88b.  888  888 888d888  8888b.  888       88888b.   .d88b.  888888 888  888  888  .d88b.  888d888 888  888        .d8888b 888d888  .d88b.   8888b.  888888 888  .d88b.  88888b.  
888 Y88b888 d8P  Y8b 888  888 888P"       "88b 888       888 "88b d8P  Y8b 888    888  888  888 d88""88b 888P"   888 .88P       d88P"    888P"   d8P  Y8b     "88b 888    888 d88""88b 888 "88b 
888  Y88888 88888888 888  888 888     .d888888 888       888  888 88888888 888    888  888  888 888  888 888     888888K        888      888     88888888 .d888888 888    888 888  888 888  888 
888   Y8888 Y8b.     Y88b 888 888     888  888 888       888  888 Y8b.     Y88b.  Y88b 888 d88P Y88..88P 888     888 "88b       Y88b.    888     Y8b.     888  888 Y88b.  888 Y88..88P 888  888 
888    Y888  "Y8888   "Y88888 888     "Y888888 888       888  888  "Y8888   "Y888  "Y8888888P"   "Y88P"  888     888  888        "Y8888P 888      "Y8888  "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                                                                                                                                
*/
//============================================================================

#include "argparse/argparse.hpp"
#include <armadillo>
#include <fstream>
#include <iostream>
#include <math.h>
#include <random>
#include <sqlite3.h>
#include <vector>

using namespace std;
using namespace arma;

string F_Hex(char *F) {

  ifstream input(F, ios::binary);

  vector<unsigned char> buffer(istreambuf_iterator<char>(input), {});
  input.close();

  char *hexStr = new char[buffer.size() * 2];
  char bufferstr[2];

  int j = 0;
  size_t sbi = 0;

  for (auto &S : buffer) {
    sprintf(hexStr + j, "%02X", (int)S);
    j += 2;
    sbi++;
  }
  hexStr[j] = '\0';

  string Return(hexStr);

  delete[] hexStr;

  return Return;
}

void Hex_F(char *hexStr, char *F) {
  string hs1, hs2;

  ofstream myfile(F, ios::binary);

  size_t Hlen = strlen(hexStr);

  for (size_t i = 0; i < Hlen; i += 2) {

    hs1 = hexStr[i];
    hs2 = hexStr[i + 1];

    myfile << (char)strtol((hs1 + hs2).c_str(), NULL, 16);
  }

  myfile.close();
}

void nerC(vector<mat> *V, vector<int> *Ln, int size) {
  for (int i = 1; i < size; i++) {
    mat N((*Ln)[i - 1], (*Ln)[i], fill::randu);

    V->push_back((N));
  }
}

void nerCB(vector<mat> *V, vector<int> *Ln, int size) {
  for (int i = 1; i < size; i++) {
    mat N(1, (*Ln)[i], fill::randu);

    V->push_back((N));
  }
}

string sql_get(sqlite3 *DB, string query) {
  sqlite3_stmt *stmt;
  if (sqlite3_prepare_v2(DB, query.c_str(), -1, &stmt, nullptr) != SQLITE_OK) {
    return "Error!";
  }

  string res;

  if (sqlite3_step(stmt) == SQLITE_ROW) {
    const char *C =
        reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    res = C;
  }

  sqlite3_finalize(stmt);
  return res;
}

struct MyArgs : public argparse::Args {

  int &id_user = kwarg("u,user", "Id user database").set_default(1);
  bool &verbose = flag("v,verbose", "A flag to toggle verbose");
};

void load_stuct(sqlite3 *DB, vector<int> *Ln, string sql_user) {
  string query1 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-start'"
                  "AND Id_User=";
  query1 += sql_user;

  string query2 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-end'"
                  "AND Id_User=";
  query2 += sql_user;

  Ln->push_back(stoi(sql_get(DB, query1.c_str())));

  for (int i = 1; i < 10; i++) {
    string queryn =
        "SELECT number_of_layers FROM Network_design_T WHERE name_t=";
    string res = sql_get(DB, queryn + "'Hidden_layer_" + to_string(i) +
                                 "' AND Id_User=" + sql_user);

    if (res.size() == 0) {
      break;
    }
    Ln->push_back(stoi(res));
  }

  Ln->push_back(stoi(sql_get(DB, query2.c_str())));
}

void save_Weights(sqlite3 *DB, vector<mat> *V, vector<mat> *B,
                  string sql_user_id) {

  string query_W = "DELETE FROM Network_Weights_M WHERE Id_User=" + sql_user_id;
  mat buf;
  sqlite3_exec(DB, query_W.c_str(), NULL, NULL, NULL);

  query_W =
      "INSERT INTO Network_Weights_M (id_Matrix, Matrix_W, Id_User)VALUES(";

  for (int i = 0; i < V->size(); i++) {
    (*V)[i].save("M.bin");

    sqlite3_exec(DB,
                 (query_W + to_string(i) + ", X'" + F_Hex("M.bin") + "', " +
                  sql_user_id + ");")
                     .c_str(),
                 NULL, NULL, NULL);
  }

  string query_B = "DELETE FROM Network_Bias_M WHERE Id_User=" + sql_user_id;

  sqlite3_exec(DB, query_B.c_str(), NULL, NULL, NULL);

  query_B = "INSERT INTO Network_Bias_M (id_Matrix, Matrix_B, Id_User)VALUES(";

  for (int i = 0; i < B->size(); i++) {
    (*B)[i].save("M.bin");
    sqlite3_exec(DB,
                 (query_B + to_string(i) + ", X'" + F_Hex("M.bin") + "', " +
                  sql_user_id + ");")
                     .c_str(),
                 NULL, NULL, NULL);

  }
}

int main(int argc, char *argv[]) {
  sqlite3 *DB;
  sqlite3_open("mydb.db", &DB);

  auto args = argparse::parse<MyArgs>(argc, argv);
  if (args.verbose)
    args.print(); // prints all variables

  arma_rng::set_seed_random();

  string sql_user = "(SELECT Login FROM 'User' WHERE Id_User = ";
  sql_user = sql_user + to_string(args.id_user) + ")";

  string sql_user_id = to_string(args.id_user);

  vector<int> Ln;

  load_stuct(DB, &Ln, sql_user);

  int size = Ln.size();

  vector<mat> V;
  vector<mat> B;

  nerC(&V, &Ln, size);
  nerCB(&B, &Ln, size);

  save_Weights(DB, &V, &B, sql_user_id);

  return 0;
}
