//============================================================================
/*

888b    888                                    888                         888                                   888                              888                   888          888    d8b                   
8888b   888                                    888                         888                                   888                              888                   888          888    Y8P                   
88888b  888                                    888                         888                                   888                              888                   888          888                          
888Y88b 888  .d88b.  888  888 888d888  8888b.  888       88888b.   .d88b.  888888 888  888  888  .d88b.  888d888 888  888        .d8888b  8888b.  888  .d8888b 888  888 888  8888b.  888888 888  .d88b.  88888b.  
888 Y88b888 d8P  Y8b 888  888 888P"       "88b 888       888 "88b d8P  Y8b 888    888  888  888 d88""88b 888P"   888 .88P       d88P"        "88b 888 d88P"    888  888 888     "88b 888    888 d88""88b 888 "88b 
888  Y88888 88888888 888  888 888     .d888888 888       888  888 88888888 888    888  888  888 888  888 888     888888K        888      .d888888 888 888      888  888 888 .d888888 888    888 888  888 888  888 
888   Y8888 Y8b.     Y88b 888 888     888  888 888       888  888 Y8b.     Y88b.  Y88b 888 d88P Y88..88P 888     888 "88b       Y88b.    888  888 888 Y88b.    Y88b 888 888 888  888 Y88b.  888 Y88..88P 888  888 
888    Y888  "Y8888   "Y88888 888     "Y888888 888       888  888  "Y8888   "Y888  "Y8888888P"   "Y88P"  888     888  888        "Y8888P "Y888888 888  "Y8888P  "Y88888 888 "Y888888  "Y888 888  "Y88P"  888  888 
                                                                                                                                                                                                                  
 */
//============================================================================

#include "argparse/argparse.hpp"
#include <armadillo>
#include <fstream>
#include <iostream>
#include <math.h>
#include <random>
#include <sqlite3.h>
#include <vector>

using namespace std;
using namespace arma;

string F_Hex(char *F) {

  ifstream input(F, ios::binary);

  vector<unsigned char> buffer(istreambuf_iterator<char>(input), {});
  input.close();

  char *hexStr = new char[buffer.size() * 2];
  char bufferstr[2];

  int j = 0;
  size_t sbi = 0;

  for (auto &S : buffer) {
    sprintf(hexStr + j, "%02X", (int)S);
    j += 2;
    sbi++;
  }
  hexStr[j] = '\0';

  string Return(hexStr);

  delete[] hexStr;

  return Return;
}

void Hex_F(string hexStr, char *F) {
  string hs1, hs2;

  ofstream myfile(F, ios::binary);

  size_t Hlen = hexStr.size();

  for (size_t i = 0; i < Hlen; i += 2) {

    hs1 = hexStr[i];
    hs2 = hexStr[i + 1];

    myfile << (char)strtol((hs1 + hs2).c_str(), NULL, 16);
  }

  myfile.close();
}

mat Nor(mat X, double Min, double Max) {
  mat F = X;

  for (int i = 0; i < X.n_elem; i++) {
    F[i] = (X[i] - Min) / (Max - Min);
  }

  return F;
}

mat ffmat(mat X, double (*f)(double)) {
  mat F = X;

  for (int i = 0; i < X.n_elem; i++) {
    F[i] = f(X[i]);
  }

  return F;
}

double dxtanh(double x) { return 1 - (x) * (x); }

void nerC(vector<mat> *V, vector<int> *Ln, int size) {
  for (int i = 1; i < size; i++) {
    mat N((*Ln)[i - 1], (*Ln)[i], fill::randu);

    V->push_back((N));
  }
}

void nerCB(vector<mat> *V, vector<int> *Ln, int size) {
  for (int i = 1; i < size; i++) {
    mat N(1, (*Ln)[i], fill::randu);

    V->push_back((N));
  }
}

vector<mat> nerO(mat I, vector<mat> V, vector<mat> B, int size) {
  vector<mat> Ov;
  mat O = ffmat(I * V[0] + B[0], tanh);
  Ov.push_back(O);

  for (int i = 1; i < size; i++) {
    O = ffmat(O * V[i] + B[i], tanh);
    Ov.push_back(O);
  }
  return Ov;
}

vector<mat> nerD(vector<mat> V, mat De, int size) {
  vector<mat> Dv;
  mat D = De;
  Dv.push_back(D);

  for (int i = size; i > 1; i--) {
    D = D * V[i - 1].t();
    Dv.push_back(D);
  }
  return Dv;
}

vector<mat> nerU(vector<mat> V, vector<mat> Dv, vector<mat> Ov, mat I,
                 int size) {
  vector<mat> Uv;
  //
  mat U = I.t() * (Dv[size - 2] % Ov[0] % ffmat(Ov[0], dxtanh));

  Uv.push_back(U);

  for (int i = 1; i < size - 1; i++) {

    U = Ov[i - 1].t() * (Dv[size - (i + 2)] % Ov[i] % ffmat(Ov[i], dxtanh));
    Uv.push_back(U);
  }

  return Uv;
}

vector<mat> nerUB(vector<mat> V, vector<mat> Dv, vector<mat> Ov, mat I,
                  int size) {
  vector<mat> Uv;
  //
  mat U = (Dv[size - 2] % Ov[0] % ffmat(Ov[0], dxtanh));

  Uv.push_back(U);

  for (int i = 1; i < size - 1; i++) {

    U = (Dv[size - (i + 2)] % Ov[i] % ffmat(Ov[i], dxtanh));
    Uv.push_back(U);
  }

  return Uv;
}

vector<mat> nerR(vector<mat> V, vector<mat> Uv, int size, const float C) {
  vector<mat> Rv;
  mat Buf;

  for (int i = 0; i < size; i++) {
    Buf = V[i] + ((Uv[i]) * C);
    Rv.push_back(Buf);
  }
  return Rv;
}

vector<mat> ERF(vector<mat> Ud1, vector<mat> Ud2) {
  vector<mat> Ur;

  for (size_t i = 0; i < Ud2.size(); i++) {
    Ur.push_back(Ud1[i] + Ud2[i]);
  }

  return Ur;
}

string sql_get(sqlite3 *DB, string query) {
  sqlite3_stmt *stmt;
  if (sqlite3_prepare_v2(DB, query.c_str(), -1, &stmt, nullptr) != SQLITE_OK) {
    return "Error!";
  }

  string res;

  if (sqlite3_step(stmt) == SQLITE_ROW) {
    const char *C =
        reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    res = C;
  }

  sqlite3_finalize(stmt);
  return res;
}

struct MyArgs : public argparse::Args {

  int &id_user = kwarg("u,user", "Id user database").set_default(1);
  int &era = kwarg("e,era", "Number of training iterations").set_default(1);
  float &speed = kwarg("s,speed", "learning rate").set_default(0.1f);
  bool &verbose = flag("v,verbose", "A flag to toggle verbose");
};

void load_stuct(sqlite3 *DB, vector<int> *Ln, string sql_user) {
  string query1 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-start'"
                  "AND Id_User=";
  query1 += sql_user;

  string query2 = "SELECT number_of_layers "
                  "FROM Network_design_T "
                  "WHERE name_t='layer-end'"
                  "AND Id_User=";
  query2 += sql_user;

  Ln->push_back(stoi(sql_get(DB, query1.c_str())));

  for (int i = 1; i < 10; i++) {
    string queryn =
        "SELECT number_of_layers FROM Network_design_T WHERE name_t=";
    string res = sql_get(DB, queryn + "'Hidden_layer_" + to_string(i) +
                                 "' AND Id_User=" + sql_user);

    if (res.size() == 0) {
      break;
    }
    Ln->push_back(stoi(res));
  }

  Ln->push_back(stoi(sql_get(DB, query2.c_str())));
}

void load_I_data(sqlite3 *DB, vector<int> *Ln, string sql_user,
                 vector<mat> *Vi, int Min, int Max) {
  string res;
  string sql = "SELECT data FROM Network_Training_T WHERE name_t='T_Data_g";

  int Gn = 1;

  for (int i = 0;; i++) {

    mat I(1, (*Ln)[0], fill::ones);

    for (int j = 0; j < (*Ln)[0]; j++) {
      res = (sql + to_string(Gn) + "' and Id_User=" + sql_user);
      I[j] = atof(sql_get(DB, res).c_str());
      Gn++;
    }

    if (sql_get(DB, res).size() == 0) {
      break;
    }

    Vi->push_back(Nor(I, Min, Max));
  }
}

void load_O_data(sqlite3 *DB, vector<int> *Ln, string sql_user,
                 vector<mat> *Ozv, int Min, int Max) {
  string res;
  string sql = "SELECT data FROM Network_Training_T WHERE name_t='T_Data_r";

  int Gn = 1;

  for (int i = 0;; i++) {

    mat Oz(1, (*Ln)[Ln->size() - 1], fill::ones);

    for (int j = 0; j < (*Ln)[Ln->size() - 1]; j++) {
      res = (sql + to_string(Gn) + "' and Id_User=" + sql_user);
      Oz[j] = atof(sql_get(DB, res).c_str());
      Gn++;
    }

    if (sql_get(DB, res).size() == 0) {
      break;
    }

    Ozv->push_back(Nor(Oz, Min, Max));
  }
}

void save_Weights(sqlite3 *DB, vector<mat> *V, vector<mat> *B,
                  string sql_user_id) {

  string query_W = "DELETE FROM Network_Weights_M WHERE Id_User=" + sql_user_id;
  mat buf;
  sqlite3_exec(DB, query_W.c_str(), NULL, NULL, NULL);

  query_W =
      "INSERT INTO Network_Weights_M (id_Matrix, Matrix_W, Id_User)VALUES(";

  for (int i = 0; i < V->size(); i++) {
    (*V)[i].save("M.bin");

    sqlite3_exec(DB,
                 (query_W + to_string(i) + ", X'" + F_Hex("M.bin") + "', " +
                  sql_user_id + ");")
                     .c_str(),
                 NULL, NULL, NULL);
  }

  string query_B = "DELETE FROM Network_Bias_M WHERE Id_User=" + sql_user_id;

  sqlite3_exec(DB, query_B.c_str(), NULL, NULL, NULL);

  query_B = "INSERT INTO Network_Bias_M (id_Matrix, Matrix_B, Id_User)VALUES(";

  for (int i = 0; i < B->size(); i++) {
    (*B)[i].save("M.bin");
    sqlite3_exec(DB,
                 (query_B + to_string(i) + ", X'" + F_Hex("M.bin") + "', " +
                  sql_user_id + ");")
                     .c_str(),
                 NULL, NULL, NULL);

  }
}

int main(int argc, char *argv[]) {

  auto args = argparse::parse<MyArgs>(argc, argv);

  if (args.verbose)
    args.print(); // prints all variables

  sqlite3 *DB;
  sqlite3_open("mydb.db", &DB);

  string sql_user = "(SELECT Login FROM 'User' WHERE Id_User = ";
  sql_user = sql_user + to_string(args.id_user) + ")";

  string sql_user_id = to_string(args.id_user);

  arma_rng::set_seed_random();

  vector<int> Ln;

  load_stuct(DB, &Ln, sql_user);

  int size = Ln.size();

  vector<mat> Vi;
  vector<mat> Ozv;

  float Silred = args.speed;

  int Min = -1;
  int Max = 1;

  int epo = args.era;

  load_I_data(DB, &Ln, sql_user, &Vi,Min,Max);

  load_O_data(DB, &Ln, sql_user, &Ozv,Min,Max);


  vector<mat> V;
  vector<mat> B;

  vector<mat> Ov;
  vector<mat> Dv;
  vector<mat> Uv;
  vector<mat> Ub;

  mat Oz;
  mat Vz;
  mat De;
  vector<mat> Nv;
  vector<mat> Nb;

  string sql = "SELECT hex(Matrix_W) FROM Network_Weights_M WHERE Id_User= " +
               sql_user_id;
  string res;
  mat buf;
  for (size_t i = 0; i < size - 1; i++) {
    res = sql_get(DB, sql + " and id_Matrix=" + to_string(i));

    Hex_F(res, "M.bin");
    buf.load("M.bin");
    V.push_back(buf);
  }

  sql =
      "SELECT hex(Matrix_B) FROM Network_Bias_M WHERE Id_User= " + sql_user_id;

  for (size_t i = 0; i < size - 1; i++) {
    res = sql_get(DB, sql + " and id_Matrix=" + to_string(i));

    Hex_F(res, "M.bin");
    buf.load("M.bin");
    B.push_back(buf);
  }

    vector<mat> UvSUM = V;
    vector<mat> UbSUM = B;

    for (size_t i = 0; i < UvSUM.size(); i++) {
      UvSUM[i] = UvSUM[i] * 0;
    }

    for (size_t i = 0; i < UbSUM.size(); i++) {
      UbSUM[i] = UbSUM[i] * 0;
    }

    mat ersu = Ozv[0] * 0;

    string query_graph = "DELETE FROM Network_graph_T WHERE Id_User=" + sql_user_id;

    sqlite3_exec(DB, query_graph.c_str(), NULL, NULL, NULL);

    mat graph_val(1, 1, fill::zeros);

    string sql_graph="INSERT INTO Network_graph_T (id_graph, data, Id_User) VALUES(";

    for (int e = 0; e < epo; e++) {

      for (size_t i = 0; i < Vi.size(); i++) {

        Ov = nerO(Vi[i], V, B, size - 1);

        De = (Ozv[i] - Ov[size - 2]);

        graph_val = graph_val + ((abs(De) * (De.t() * 0 + 1)));

        ersu = ersu + (Ozv[i] - Ov[size - 2]) % (Ozv[i] - Ov[size - 2]);

        Dv = nerD(V, De, size - 1);

        Uv = nerU(V, Dv, Ov, Vi[i], size);

        Ub = nerUB(V, Dv, Ov, Vi[i], size);

        UvSUM = ERF(UvSUM, Uv);
        UbSUM = ERF(UbSUM, Ub);
      }

      sqlite3_exec(DB, (sql_graph+to_string(e)+", "+to_string(graph_val[0]/Vi.size())+", "+sql_user_id+")").c_str(), NULL, NULL, NULL);

            graph_val=graph_val*0;

      Nv = nerR(V, UvSUM, size - 1, Silred);
      Nb = nerR(B, UbSUM, size - 1, Silred);

      V = Nv;
      B = Nb;

      for (size_t i = 0; i < UvSUM.size(); i++) {
        UvSUM[i] = UvSUM[i] * 0;
      }

      for (size_t i = 0; i < UbSUM.size(); i++) {
        UbSUM[i] = UbSUM[i] * 0;
      }
    }

    for (size_t i = 0; i < Vi.size(); i++) {
      Ov = nerO(Vi[i], V, B, size - 1);
      cout << (Ov[size - 2] * (Max - Min) + Min) << endl;
    }

    save_Weights(DB, &V, &B, sql_user_id);
  return 0;
}
